#!/bin/bash

set -e
set -u

remote_uboot_overlay_add() {
	target_addr="$1"
	overlay_nr="$2"
	overlay_file="$3"
	ssh root@$target_addr "sed -i -E 's/#(uboot_overlay_addr$overlay_nr)=(\/lib\/firmware)\/<file$overlay_nr>\.dtbo/\1=\2\/$overlay_file/g' /boot/uEnv.txt"
}

main() {
	if [ $# != 1 ]; then
		echo "USAGE: $0 target-ipaddr"
		exit 1
	fi
	target_addr="$1"

	remote_uboot_overlay_add $target_addr 1 BB-UART2-00A0.dtbo
	remote_uboot_overlay_add $target_addr 2 BB-UART4-00A0.dtbo
	remote_uboot_overlay_add $target_addr 3 BB-I2C1-00A0.dtbo
}

main $@
