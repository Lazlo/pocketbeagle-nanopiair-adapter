# Pocketbeagle to NanoPi NEO Air Adapter

Circuit to replace [NanoPi NEO Air](http://wiki.friendlyarm.com/wiki/index.php/NanoPi_NEO_Air) with a [Pocketbeagle](https://github.com/beagleboard/pocketbeagle)

## Status

Status for pins connected between Pocketbeagle and NanoPi.

| PB    | Nano     | Status   |
| ----- | -------- | -------- |
| UART0 | UART0    | working  |
| UART2 | UART1    | working  |
| UART4 | UART2    | working  |
| SPI1  | SPI0     | untested |
| I2C1  | I2C0     | not okay |
| USB1  | USB2     | not okay |
| CAN0  | LRCK/BCK | untested |
